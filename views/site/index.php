<?php

use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */

$this->title = 'My Awesome Exam';
?> 

<h1>Examen Final Práctico</h1>

<div class="separador"></div>
<div class="site-index ">

    <section> 
        <h2><?=$enunciado?></h2>
            <?= GridView::widget([

                    'dataProvider' => $dataProvider,
                    'columns' => [
                        'dorsal',
                        'nombre',
                    ],
                ]); ?>
         </section>

 
    <section>  
        <h2><?=$enunciado2?></h2>
           <?= GridView::widget([

                  'dataProvider' => $dataProvider2,
                  'columns' => [
                      'nompuerto',
                      'dorsal',
                  ],
              ]); ?> 
        
         <h2 class="espacio"><?=$enunciado1?></h2>

           <?= GridView::widget([

                   'dataProvider' => $dataProvider1,
                   'columns' => [
                       'numetapa',
                       'kms',
                   ],
               ]); ?> 
    </section>
    
        
     
</div>
